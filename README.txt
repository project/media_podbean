
/*********************/
 Media: Podbean
/*********************/

Author: Gareth Alexander (the_g_bomb)
Maintainer: Gareth Alexander (the_g_bomb)

Requires: Drupal 6, Embedded Media Field (emfield), Embedded Audio Field (emaudio)

/*********************/
 Installation
/*********************/

Take the entire folder downloaded and add it to your modules directory. Then go to admin/build/modules and enable the module.

/*********************/
 Configuration
/*********************/

Set the global configuration options

1. Go to Content Management > Embedded Media Field configuration at: /admin/content/emfield
2. Click on the audio tab or go to: /admin/content/emfield/emaudio
3. Under Providers find the Podbean Configuration fieldset and click Podbean configuration to expand it
4. Put a tick into the checkbox "Allow content from Podbean".
5. Choose the player you want to use from Light Flash Player, Dark Flash Player or HTML5 as per the podbean site and click Save configuration.

Set the field configration options

1. Add the emfield element to your content type and choose "Embedded Audio" for the Type of data to store and 3rd Party Audio for the Form element to edit the data and click Save.
2. Under "Providers Supported" there will be a list of providers that you enable in the global options.
3. Tick the checkbox next to Podbean under providers and click Save

Adding media

1. Create content using your embedded media enable content type and add the media into the content.
2. Go to your podcast homepage (http://userid.podbean.com/) which should display a list of your podcasts.
3. Each of the podcasts has an Embeddable Player link which takes you to a page where you can copy the embed code,
or the Download link takes you to a page where you can get the media URL, although you should also be able to get the download link with right click/ctrl click > Copy link location.
4. Paste either of these codes into the embedded media field and click save.
