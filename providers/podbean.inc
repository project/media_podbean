<?php

/*
 * @file
 * This include processes Podbean audio files for use by emaudio.module.
 */

define('EMAUDIO_PODBEAN_MAIN_URL', 'http://www.podbean.com/');
define('EMAUDIO_PODBEAN_DATA_VERSION', 1);

/**
 * Implementation of hook_emaudio_PROVIDER_info().
 *
 * This returns information relevant to a specific 3rd party audio provider.
 *
 * @return
 *   An array of strings requested by various admin and other forms.
 *   'name' => The translated name of the provider.
 *   'url' => The url to the main page for the provider.
 *   'settings_description' => A description of the provider.
 *   'supported_features' => An array describing supported features.
 */
function emaudio_podbean_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS attachment'), t('No'), ''),
    array(t('Thumbnails'), t('No'), t('')),
    array(t('Choice of player'), t('Yes'), t("You may choose which player to use from a list of available players.")),
  );
  return array(
    'provider' => 'podbean',
    'name' => t('Podbean'),
    'url' => EMAUDIO_PODBEAN_MAIN_URL,
    'settings_description' => t('These settings specifically affect audio displayed from <a href="@podbean">Podbean</a>', array('@podbean' => EMAUDIO_PODBEAN_MAIN_URL)),
    'supported_features' => $features,
  );
}

/**
 *  Implement hook_emvideo_PROVIDER_data_version().
 */
function emaudio_podbean_data_version() {
  return EMAUDIO_PODBEAN_DATA_VERSION;
}

/**
 * Implementation of hook_emaudio_PROVIDER_settings().
 *
 * Returns a subform to be added to the emaudio_settings() admin settings page.
 * Note: a form field is already provided.
 * Specific provider settings to be added to that form field.
 */
function emaudio_podbean_settings() {
  $form = array();

  $form['podbean']['podbean_player_style'] = array(
    '#type' => 'select',
    '#title' => t('Podbean player style'),
    '#options' => array('light' => 'Light Flash Player', 'dark' => 'Dark Flash Player', 'html5' => 'HTML5'),
    '#default_value' => variable_get('podbean_player_style', 'light'),
  );

  return $form;
}

/**
 * Implementation of hook_emaudio_PROVIDER_extract().
 *
 * This is called to extract the video code from a pasted URL or embed code.
 *
 * @param $embed
 *   An optional string with the pasted URL or embed code.
 * @return
 *   Either an array of regex expressions to be tested, or a string with the
 *   audio code to be used. If the hook tests the code itself, it should return
 *   either the string of the audio code (if matched), or an empty array.
 *   Otherwise, the calling function will handle testing the embed code against
 *   each regex string in the returned array.
 */
function emaudio_podbean_extract($embed = '') {
  // Can be in the form of:
  // http://www.podbean.com/podcast-download?b=000000&f=http://membername.podbean.com/mf/web/x0x0x0/filename.mp3
  // http://membername.podbean.com/mf/play/x0x0x0/filename.mp3
  return array(
    '@podbean.com\/mf\/web\/([^"\&]+)@i',
    '@podbean.com\/mf\/play\/([^"\&]+)@i',
  );
}

/**
 * Implementation of hook_PROVIDER_data().
 */
function emaudio_podbean_data($field, $item) {
  $data = array();
  if (preg_match('@http://(\w+).podbean.com\/mf\/(web|play)\/(([^/]+)/([^/]+))@i', $item['embed'], $matches)) {
    $data = array(
      'userid' => $matches[1],
      'uniquecode' => $matches[4],
      'file' => $matches[5],
      'media_url' => $matches[1] .'.podbean.com/mf/play/'. $matches[4] .'/'. $matches[5],
      'download_url' => $matches[1] .'.podbean.com/mf/web/'. $matches[4] .'/'. $matches[5],
      'title' => '',
    );
    $data['emimage_podbean_version'] = EMAUDIO_PODBEAN_DATA_VERSION;
  }
  return $data;
}

/**
 * The embedded flash displaying the podbean audio.
 */
function theme_emaudio_podbean_flash($item, $embed, $width, $height, $autoplay, $preview = FALSE) {
  $output = '';
  if ($autoplay != 0) {
    $auto_play = 'yes';
  }
  else {
    $auto_play = 'no';
  }
  $media_url = $item['media_url'];
//  $media_url = check_url($item['userid'] .'.podbean.com/mf/play/'. $embed);
  
  $output = '<div>';
  switch (variable_get('podbean_player_style', 'light')) {
    case 'default':
    case 'light':
      $output .= '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="210" height="25" id="mp3playerlightsmallv3" align="middle">';
      $output .= '<param name="allowScriptAccess" value="sameDomain" />';
      $output .= '<param name="movie" value="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerlightsmallv3.swf?audioPath=';
      $output .= $media_url .'&autoStart='. $auto_play .'" />';
      $output .= '<param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><param name="wmode" value="transparent" />';
      $output .= '<embed src="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerlightsmallv3.swf?audioPath=';
      $output .= $media_url .'&autoStart='. $auto_play .'" quality="high"  width="210" height="25" name="mp3playerlightsmallv3" align="middle" allowScriptAccess="sameDomain" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" /></embed>';
      $output .= '</object><br />';
      break;
    case 'dark':
      $output = '<div>';
      $output .= '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="210" height="25" id="mp3playerdarksmallv3" align="middle">';
      $output .= '<param name="allowScriptAccess" value="sameDomain" />';
      $output .= '<param name="movie" value="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerdarksmallv3.swf?audioPath=';
      $output .= $media_url .'&autoStart='. $auto_play .'" />';
      $output .= '<param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><param name="wmode" value="transparent" />';
      $output .= '<embed src="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerdarksmallv3.swf?audioPath=';
      $output .= $media_url .'&autoStart='. $auto_play .'" quality="high"  width="210" height="25" name="mp3playerdarksmallv3" align="middle" allowScriptAccess="sameDomain" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" /></embed>';
      $output .= '</object><br />';
      break;
    case 'html5':
      $output = '<div>';
      $output .= '<audio controls="controls" id="auidoplayerhtml5podbean49c86e34c6eee3348ee77141c59b1d83">';
      $output .= '<source src="'. $media_url .'" type="audio/mpeg" autoplay="'. $auto_play .'">';
      $output .= 'Your browser does not support the audio element.';
      $output .= '</audio>';
      $output .= '<script type="text/javascript">';
      $output .= 'var audioTag = document.createElement(\'audio\');';
      $output .= 'if (!(!!(audioTag.canPlayType) && ("no" != audioTag.canPlayType("audio/mpeg")) && ("" != audioTag.canPlayType("audio/mpeg")))) {';
      $output .= 'document.getElementById(\'auidoplayerhtml5podbean49c86e34c6eee3348ee77141c59b1d83\').parentNode.removeChild(document.getElementById(\'auidoplayerhtml5podbean49c86e34c6eee3348ee77141c59b1d83\'));';
      $output .= 'document.write(\'<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="210" height="25" id="mp3playerlightsmallv3" align="middle"><param name="allowScriptAccess" value="sameDomain" /><param name="movie" value="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerlightsmallv3.swf?audioPath='. $embed .'&autoStart='. $auto_play .'" /><param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><param name="wmode" value="transparent" /><embed src="http://www.podbean.com/podcast-audio-video-blog-player/mp3playerlightsmallv3.swf?audioPath='. $embed .'&autoStart='. $auto_play .'" quality="high"  width="210" height="25" name="mp3playerlightsmallv3" align="middle" allowScriptAccess="sameDomain" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" /></embed></object>\');';
      $output .= '}';
      $output .= '</script><br />';
      break;
  }
  $output .= l(t('Podcast Powered By Podbean'), EMAUDIO_PODBEAN_MAIN_URL, array('attributes' => array('style' => 'font-family: arial, helvetica, sans-serif; font-size: 11px; font-weight: normal; padding-left: 41px; color: #2DA274; text-decoration: none; border-bottom: none;'))) .'<br />';
  $output .= l(t('Download this podcast'), $item['download_url'], array('attributes' => array('style' => 'font-family: arial, helvetica, sans-serif; font-size: 11px; font-weight: normal; padding-left: 41px; color: #2DA274; text-decoration: none; border-bottom: none;')));
  $output .= '</div>';
  return $output;
}

/**
 * Implementation of hook_emaudio_PROVIDER_audio().
 *
 * Displays the full/normal-sized video, usually on the default page view.
 *
 * @param $embed
 *   The video code for the audio to embed.
 * @param $width
 *   The width to display the audio.
 * @param $height
 *   The height to display the audio.
 * @param $field
 *   The field info from the requesting node.
 * @param $item
 *   The actual content from the field.
 * @return
 *   The html of the embedded audio.
 */
function emaudio_podbean_audio($embed, $width, $height, $field, $item, $node, $autoplay) {
  return theme('emaudio_podbean_flash', $item, $embed, $width, $height, $autoplay);
}

/**
 * Implementation of hook_emaudio_PROVIDER_preview().
 *
 * Displays the preview-sized audio we want, commonly for the teaser.
 *
 * @param $embed
 *   The audio code for the audio to embed.
 * @param $width
 *   The width to display the audio.
 * @param $height
 *   The height to display the audio.
 * @param $field
 *   The field info from the requesting node.
 * @param $item
 *   The actual content from the field.
 * @return
 *   The html of the embedded audio.
 */
function emaudio_podbean_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  return theme('emaudio_podbean_flash', $item, $embed, $width, $height, $autoplay, TRUE);
}

/**
 * Implementation of hook_emfield_subtheme().
 */
function emaudio_podbean_emfield_subtheme() {
  return array(
    'emaudio_podbean_flash' => array(
      'arguments' => array('item' => NULL, 'embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL, 'preview' => FALSE),
      'file' => 'providers/podbean.inc'
    )
  );
}
